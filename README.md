# momoviz (MoreMoney Visualizer)

## Concept

Input financial parameters to see dynamic visualizations of potential financial growth over time.

## To-Do List

WIP

## Current Status

Concept phase. Python script for calculations exists, web implementation not started.

## PoC in Python
**Example: €70k initial capital with €2k monthly budget, 20% return rate for investments, 3% for funds, 8% for crypto, and a tax rate of 25%.**

![Calculation concept in Python](image.png)

## Financial Projection Formula

The following formula is used to project the future capital over a specified number of years.

### Inputs

- $( C_0 )$: Initial total capital
- $( M )$: Monthly budget (amount saved and invested)
- $( P_s )$: Percentage allocated to savings
- $( P_i )$: Percentage allocated to investments
- $( P_c )$: Percentage allocated to cryptocurrency
- $( r_s )$: Annual return rate on savings
- $( r_i )$: Annual return rate on investments
- $( r_c )$: Annual return rate on cryptocurrency
- $( n )$: Number of years

### Monthly Contributions

- $( M_s = M \times P_s )$
- $( M_i = M \times P_i )$
- $( M_c = M \times P_c )$

### Yearly Growth Calculation

For each year, update the total capital based on the returns and new contributions.

### Formula
$S_{n} = S_{n-1} \times (1 + r_s) + M_s \times 12$

$I_{n} = I_{n-1} \times (1 + r_i) + M_i \times 12$

$C_{n} = C_{n-1} \times (1 + r_c) + M_c \times 12$

Where ( S ), ( I ), and ( C ) represent the savings, investments, and cryptocurrency values respectively for year ( n ).
