#!/usr/bin/python

import csv

# Adjust Values:
initial_capital = 70000
monthly_budget = 2000
perc_savings = 0.37
perc_investments = 0.60
perc_crypto = 0.03
return_savings = 0.03
return_investments = 0.20
return_crypto = 0.08
tax_rate = 0.25  
years = 10

def calculate_future_capital(initial_capital, monthly_budget, perc_savings, perc_investments, perc_crypto, 
                             return_savings, return_investments, return_crypto, tax_rate, years, 
                             export_csv=False, csv_filename="future_capital.csv"):

    # Initial split of the capital
    savings = initial_capital * perc_savings
    investments = initial_capital * perc_investments
    crypto = initial_capital * perc_crypto
    
    # Monthly contributions
    monthly_savings = monthly_budget * perc_savings
    monthly_investments = monthly_budget * perc_investments
    monthly_crypto = monthly_budget * perc_crypto
    
    # Prepare data for CSV and Markdown
    csv_data = [["Year", "Savings", "Investments", "Crypto", "Total (No Returns)", "Total (Before Tax)", "Total (After Tax)"]]
    markdown_data = [
        "| Year | Savings      | Investments  | Crypto       | Total (No Returns) | Total (Before Tax) | Total (After Tax) |",
        "|------|--------------|--------------|--------------|--------------------|--------------------|-------------------|"
    ]
    
    # Add initial values
    total_capital = savings + investments + crypto
    total_no_returns = total_capital
    csv_data.append([0, f"{savings:.2f}", f"{investments:.2f}", f"{crypto:.2f}", f"{total_no_returns:.2f}", f"{total_capital:.2f}", f"{total_capital:.2f}"])
    markdown_data.append(f"| {0:4} | {savings:12.2f} | {investments:12.2f} | {crypto:12.2f} | {total_no_returns:18.2f} | {total_capital:18.2f} | {total_capital:17.2f} |")
    
    for year in range(1, years + 1):
        # Update no returns total
        total_no_returns += (monthly_savings + monthly_investments + monthly_crypto) * 12
        
        # Update values (before tax)
        new_savings = savings * (1 + return_savings) + monthly_savings * 12
        new_investments = investments * (1 + return_investments) + monthly_investments * 12
        new_crypto = crypto * (1 + return_crypto) + monthly_crypto * 12
        
        # Calculate gains
        savings_gain = new_savings - savings - monthly_savings * 12
        investments_gain = new_investments - investments - monthly_investments * 12
        crypto_gain = new_crypto - crypto - monthly_crypto * 12
        
        # Calculate tax (only on gains)
        tax = (savings_gain + investments_gain + crypto_gain) * tax_rate
        
        # Update values
        savings = new_savings
        investments = new_investments
        crypto = new_crypto
        
        # Calculate totals
        total_before_tax = savings + investments + crypto
        total_after_tax = total_before_tax - tax
        
        # Add to data structures
        csv_data.append([year, f"{savings:.2f}", f"{investments:.2f}", f"{crypto:.2f}", 
                         f"{total_no_returns:.2f}", f"{total_before_tax:.2f}", f"{total_after_tax:.2f}"])
        markdown_data.append(f"| {year:4} | {savings:12.2f} | {investments:12.2f} | {crypto:12.2f} | "
                             f"{total_no_returns:18.2f} | {total_before_tax:18.2f} | {total_after_tax:17.2f} |")
    
    # Print Markdown data
    for line in markdown_data:
        print(line)
    
    # Export to CSV
    if export_csv:
        with open(csv_filename, mode='w', newline='') as file:
            writer = csv.writer(file)
            writer.writerows(csv_data)
        print(f"\nData exported to {csv_filename}")

calculate_future_capital(initial_capital, monthly_budget, perc_savings, perc_investments, perc_crypto, 
                         return_savings, return_investments, return_crypto, tax_rate, years, 
                         export_csv=True, csv_filename="future_capital.csv")
